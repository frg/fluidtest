#!/bin/bash

# This script will generate the baseline for unit test: test2

./run.test2
mv -f *.log ../baseline/test2
mv -f *.res ../baseline/test2
mv -f *.dat ../baseline/test2
