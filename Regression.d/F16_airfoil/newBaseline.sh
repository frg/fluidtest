#!/bin/bash

# This script will generate the baseline for unit test: F16_airfoil

./run.F16_airfoil
mv -f *.log ../baseline/F16_airfoil
mv -f *.dat ../baseline/F16_airfoil
