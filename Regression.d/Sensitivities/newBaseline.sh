#!/bin/bash

# This script will generate the baseline for sensitivity test

./run.Sensitivities
cp -f log ../baseline/Sensitivities/
cp -f res ../baseline/Sensitivities/
cp -f *.dat ../baseline/Sensitivities/
