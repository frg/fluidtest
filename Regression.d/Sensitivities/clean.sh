#!/bin/bash

rm -f log res *.dat Cylinder_coarse.timing
rm -f references/*
rm -f data/*
rm -f Positions.d/*
rm -f sources/Cylinder_Coarse.xpost.dec.60 sources/cylinder_coarse.match.*
