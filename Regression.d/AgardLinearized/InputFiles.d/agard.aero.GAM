under Problem {
  Type = GAMConstruction;
  Mode = Dimensional;  
}

under Input {
  Prefix = "DataFiles.d/";
  Connectivity = "agard.con";
  Geometry = "agard.msh";
  Decomposition = "agard.dec";
  CpuMap = "agard.8cpu";
  Matcher = "agard.match";
  Solution = "../Results.d/Fluid.d/agard.m0.97.ss.sol";
}

under Output {
  under Postpro {
    Prefix = "Results.d/Fluid.d/";
    GAMData = "agard.m0.97.gamData";
    GAMFData = "agard.m0.97.gamFData";
    Frequency = 1;
  }
}

under Linearized  {
  Amplification = 0.1;
  Eps = 1e-4;
  NumStrModes = 4;
  StrModes = "Results.d/Fluid.d/agard.mpp.4modes.pos";
  Domain = Frequency;
  GAMReducedFrequency1 = 0.;
  GAMReducedFrequency2 = 0.015;
  GAMReducedFrequency3 = 0.05;
}

under Equations {
  Type = Euler;
  under FluidModel[0]{
    Fluid = PerfectGas;
  }
}

under BoundaryConditions {
  under Inlet {
    Mach = 0.97;
    Alpha = 0.0; 
    Beta = 0.0;
    Density = 0.61115933E-7;  // slugs / in^3
    Pressure = 6.0;//            // psi*12 ([slugs/(in-s^2)])
  }
}

under Space {
  under NavierStokes {
    Flux = Roe; 
    Reconstruction = Linear;
    Limiter = VanAlbada;
    Gradient = Galerkin;
  }
}

under Time {
  Type = Implicit;
  Form = Descriptor;
  TypeTimeStep = Global;
  TimeStep = 0.0001;
  MaxIts = 20000;
  Eps = 1.0e-8;  
  under Implicit {
    Type = ThreePointBackwardDifference;
    under Newton { 
      MaxIts = 1; 
      Eps = 1e-05;
      FailSafe = On;
      under LinearSolver {
        under NavierStokes {
          Type = Gmres;
          MaxIts = 500;
          KrylovVectors = 500;
          Eps = 1e-05;
          //Output = "stdout";
          under Preconditioner { Type = Ras; Fill = 0; }
        }
      }
    }
  }
}

under MeshMotion {

  Type = Basic;
  Element = TorsionalSprings;

  under Newton  {
    MaxIts = 1;
    under LinearSolver  {
      Type = Cg;
      MaxIts = 1000;
      KrylovVectors = 1000;
      Eps = 1e-9;
      //Output = "stdout";
      under Preconditioner { Type = Jacobi; Fill = 0; }
    }
  }
}
