under Problem {
  Type = UnsteadyAeroelastic;
  Mode = Dimensional;
}

under Input {
  Prefix = "DataFiles.d/";
  Connectivity = "agard.con";
  Geometry = "agard.msh";
  Decomposition = "agard.dec";
  CpuMap = "agard.4cpu";
  Matcher = "agard.match";
}

under Output {
  under Restart { 
    Prefix = "Results.d/Fluid.d/";
    Position = "agard.pp.mode1.pos";
    Frequency = 0;
  }
}

under Equations {
  Type = Euler;
  under FluidModel[0]{
    Fluid = PerfectGas;
  }
}

under BoundaryConditions {
  under Inlet {
    Mach = 0.9;
    Alpha = 0.0; 
    Beta = 0.0;
    Density = 2.7400E-8;
    Pressure = 2.73;
  }
}

under Space {
  under NavierStokes { 
    Reconstruction = Linear;
    Limiter = VanAlbada;
  }
}

under Time {
  Type = Implicit;
  MaxIts = 1;
  Cfl0 = 1000.0;
  CflMax = 1000.0;
  Eps = 1.0e-8;  
  under Implicit {
    Type = ThreePointBackwardDifference;
    MatrixVectorProduct = Exact;
    under Newton { 
      MaxIts = 1; 
      Eps = 1e-5;
      FailSafe = On;
      under LinearSolver {
        under NavierStokes {
          Type = Gmres;
          MaxIts = 100;
          KrylovVectors = 100;
          Eps = 0.0001;
          //Output = "stdout";
          under Preconditioner { Type = Ras; Fill = 0; }
        }
      }
    }
  }
}

under MeshMotion {

  Type = Basic;
  Element = TorsionalSprings;


  under Newton  {
    MaxIts = 1;
    under LinearSolver  {
      Type = Cg;
      MaxIts = 1000;
      KrylovVectors = 1000;
      Eps = 1e-9;
      Output = "stdout";
      under Preconditioner { Type = Jacobi; Fill = 0; }
    }
  }
}
