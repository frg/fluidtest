#!/bin/bash

# This script will generate the baseline for unit test: AgardLinearized

./run.AgardLinearized
mv -f *.log ../baseline/AgardLinearized
mv -f *.dat ../baseline/AgardLinearized
