#!/bin/bash

# This script will generate the baseline for unit test: EMBViscous

./run.EMBViscous
mv -f *.dat ../baseline/EMBViscous
mv -f simulations/Run/log/aerof.out ../baseline/EMBViscous/Run_aerof.out
mv -f simulations/Run/log/NSconv.out ../baseline/EMBViscous/Run_NSconv.out
mv -f simulations/Run/log/Turbconv.out ../baseline/EMBViscous/Run_Turbconv.out
mv -f simulations/Run/results/Residual.out ../baseline/EMBViscous/Run_Residual.out
mv -f simulations/Run_FD/log/aerof.out ../baseline/EMBViscous/Run_FD_aerof.out
mv -f simulations/Run_FD/log/NSconv.out ../baseline/EMBViscous/Run_FD_NSconv.out
mv -f simulations/Run_FD/log/Turbconv.out ../baseline/EMBViscous/Run_FD_Turbconv.out
mv -f simulations/Run_FD/results/Residual.out ../baseline/EMBViscous/Run_FD_Residual.out
mv -f simulations/Run_FO/log/aerof.out ../baseline/EMBViscous/Run_FO_aerof.out
mv -f simulations/Run_FO/log/NSconv.out ../baseline/EMBViscous/Run_FO_NSconv.out
mv -f simulations/Run_FO/log/Turbconv.out ../baseline/EMBViscous/Run_FO_Turbconv.out
mv -f simulations/Run_FO/results/Residual.out ../baseline/EMBViscous/Run_FO_Residual.out
mv -f simulations/Run_Heaving/log/aerof.out ../baseline/EMBViscous/Run_Heaving_aerof.out
mv -f simulations/Run_Heaving/log/NSconv.out ../baseline/EMBViscous/Run_Heaving_NSconv.out
mv -f simulations/Run_Heaving/log/Turbconv.out ../baseline/EMBViscous/Run_Heaving_Turbconv.out
mv -f simulations/Run_Heaving/results/Residual.out ../baseline/EMBViscous/Run_Heaving_Residual.out
mv -f simulations/Run_HLLC/log/aerof.out ../baseline/EMBViscous/Run_HLLC_aerof.out
mv -f simulations/Run_HLLC/log/NSconv.out ../baseline/EMBViscous/Run_HLLC_NSconv.out
mv -f simulations/Run_HLLC/log/Turbconv.out ../baseline/EMBViscous/Run_HLLC_Turbconv.out
mv -f simulations/Run_HLLC/results/Residual.out ../baseline/EMBViscous/Run_HLLC_Residual.out
mv -f simulations/Run_LM/log/aerof.out ../baseline/EMBViscous/Run_LM_aerof.out
mv -f simulations/Run_LM/log/NSconv.out ../baseline/EMBViscous/Run_LM_NSconv.out
mv -f simulations/Run_LM/log/Turbconv.out ../baseline/EMBViscous/Run_LM_Turbconv.out
mv -f simulations/Run_LM/results/Residual.out ../baseline/EMBViscous/Run_LM_Residual.out
