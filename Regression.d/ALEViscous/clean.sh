#!/bin/bash

rm -f *.dat
rm -f simulations/Run/postpro/Pressure.* simulations/Run/results/* simulations/Run/log/* simulations/Run/references/*
rm -f simulations/Run_FD/postpro/Pressure.* simulations/Run_FD/results/* simulations/Run_FD/log/* simulations/Run_FD/references/*
rm -f simulations/Run_FO/postpro/Pressure.* simulations/Run_FO/results/* simulations/Run_FO/log/* simulations/Run_FO/references/*
rm -f simulations/Run_Heaving/postpro/Pressure.* simulations/Run_Heaving/results/* simulations/Run_Heaving/log/* simulations/Run_Heaving/references/*
rm -f simulations/Run_HLLC/postpro/Pressure.* simulations/Run_HLLC/results/* simulations/Run_HLLC/log/* simulations/Run_HLLC/references/*
rm -f simulations/Run_LM/postpro/Pressure.* simulations/Run_LM/results/* simulations/Run_LM/log/* simulations/Run_LM/references/*
rm -f reg.out
