under Problem{
  Type = Steady;
  Mode = NonDimensional;
  Prec = NonPreconditioned;
}

under Input {
  GeometryPrefix = "../../data/mesh";
//  Solution = "references/Solution.bin";
//  RestartData = "references/Restart.data";
}
                                                                                                 
under Output {
  under Postpro {
    Prefix = "postpro/";
//    Density = "Density.bin";
//    Velocity = "Velocity.bin";
    Pressure = "Pressure.bin";
    LiftandDrag = "../results/LiftandDrag.out";
    Residual = "../results/Residual.out";
//    Force = "../results/Force.out";
    Frequency = 0;
  }
  under Restart {
    Frequency = 0;
    Prefix = "references/";
    Solution = "Solution.bin";
    RestartData = "Restart.data";
  }
}

under Equations {
  Type = NavierStokes;
  under FluidModel[0] {
    Fluid = PerfectGas;
    under GasModel{
      SpecificHeatRatio = 1.4;
      PressureConstant = 0.0;
    }
  }
  under ViscosityModel {
    Type = Sutherland;
    SutherlandConstant = 1.0;
    SutherlandReferenceTemperature = 110.6;
  }
  under TurbulenceClosure{
    Type = TurbulenceModel;
    under TurbulenceModel{
      Type = SpalartAllmaras;
    }
  }
}

under ReferenceState {
  Length = 1.0;
  Mach = 0.3;
  Reynolds = 1.0e5;
  Temperature = 273.15;
}

under BoundaryConditions {
  under Inlet {
    Type = External;
    Mach = 0.3;
    Density = 1.0;
    Alpha = 0.0;
    Beta  = 5.0;
    NuTilde = 0.1;
  }
  under Wall {
    Type = Adiabatic;
    Integration = Full;
  }
}

under Space {
  under NavierStokes {
    Flux = Roe;
    Reconstruction = Linear;
    AdvectiveOperator = FiniteVolume;
    Limiter = VanAlbada;
    Gradient = LeastSquares;
    Dissipation = SecondOrder;
    Beta = 0.33333333333;
    Gamma = 1.0;
  }
  under Boundaries {
    Type = StegerWarming;
  }
}

under Time {
  Prec = NonPreconditioned;
  Type = Implicit;
  MaxIts = 10000;
  Eps  = 1.0e-7;
  Ser  = 0.7;
  Cfl0 = 0.0; //  cfl = min( max( max(cflCoef1, cflCoef2*its), cfl0/pow(res,ser) ), cflMax );
  Cfl1 = 2.0;
  Cfl2 = 2.0;
  CflMax = 200.;
  under Implicit {
    MatrixVectorProduct = FiniteDifference;
    TurbulenceModelCoupling = Weak;
    under Newton {
      MaxIts = 1;
      Eps = 0.01;
      FailSafe = AlwaysOn;
      under LinearSolver {
        under NavierStokes {
          Type = Gmres;
          MaxIts = 250;
          KrylovVectors = 250;
          Eps = 1.e-4;
          Output = "log/NSconv.out";
          under Preconditioner {
            Type = Ras;
            Fill = 0;
          }
        }
        under TurbulenceModel {
          Type = Gmres;
          MaxIts = 150;
          KrylovVectors = 150;
          Eps = 1.e-4;
          Output = "log/Turbconv.out";
          under Preconditioner {
            Type = Ras;
            Fill = 0;
          }
        }
      }
    }
  }
}
