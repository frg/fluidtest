#!/bin/bash

MESHPATH="../../../data"
SOWER=../../../../sower

function extract {
  if [ $# == 0 ]; then
    echo "Usage: $0 binary_msh_prefix <list of binary result_files>"
  elif [ $# == 1 ]; then
    $SOWER -fluid -merge -mesh $MESHPATH/$1.msh -con $MESHPATH/$1.con -dec $MESHPATH/$1.dec;
  else
    for i in $@; do 
       if [ -a $i.bin001 ]; then
         $SOWER -fluid -merge -mesh $MESHPATH/$1.msh -con $MESHPATH/$1.con -dec $MESHPATH/$1.dec -result $i.bin -output $i -width 12 -precision 6;
       fi
    done
  fi
}

echo "Extracting Mesh..."
extract mesh

echo "Extracting Flow Solution..."
extract mesh Pressure
