#!/bin/bash

# This script will generate the baseline for unit test: test1

./run.test1
mv -f *.log ../baseline/test1
mv -f *.res ../baseline/test1
mv -f *.dat ../baseline/test1
