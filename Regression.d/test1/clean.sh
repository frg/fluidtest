#!/bin/bash

rm -f residuals plate.optDec plate.timing *.dat *.log *.res reg.out log
rm -f Matcher/OUPUT.* SowerF/OUTPUT.* SowerS/OUTPUT.* MeshF/*.dec.* 
rm -f ResultsF/* ResultsS/* RestartF/* RestartS/*
