under Problem{
  Type = Forced;
  Mode = NonDimensional;
  Prec = NonPreconditioned;
  Framework = EmbeddedALE;
  SolveFluid = Off;
}

under Input {
  GeometryPrefix = "../data/cyl";
  EmbeddedSurface = "../sources/Surf.top";
//  Solution = "references/Solution.bin";
//  Position = "references/Position.bin";
//  EmbeddedPosition = "references/EmbeddedPosition.dat";
//  RestartData = "references/Restart.data";
}
                                                                                                 
under Output {
  under Postpro {
    Prefix = "postpro/";
//    Density = "Density.bin";
//    Velocity = "Velocity.bin";
//    Pressure = "Pressure.bin";
    Displacement = "Displacement.bin";
    EmbeddedSurfaceDisplacement = "EmbeddedDisplacement.dat";
//    FluidID = "FluidID.bin";
//    LiftandDrag = "../results/LiftandDrag.out";
//    Residual = "../results/Residual.out";
//    Force = "../results/Force.out";
    Frequency = 0;
  }
  under Restart {
    Frequency = 0;
    Prefix = "references/";
    Solution = "Solution.bin";
    Position = "Position.bin";
    EmbeddedPosition = "EmbeddedPosition.dat";
    RestartData = "Restart.data";
  }
}

under Equations {
  Type = NavierStokes;
  under FluidModel[0] {
    Fluid = PerfectGas;
    under GasModel{
      SpecificHeatRatio = 1.4;
      PressureConstant = 0.0;
    }
  }
  under ViscosityModel {
    Type = Constant;//Sutherland;//Constant;
    SutherlandReferenceTemperature = 110.6;
    SutherlandConstant = 1.0; //1.458e-6;
  }
}

under ReferenceState {
  Length = 1.0;
  Mach = 0.3;
  Reynolds = 100.;
  Temperature = 273.15;
}

under BoundaryConditions {
  under Inlet {
    Type = External;
    Mach = 0.3;
    Density = 1.0;
    Alpha = 0.0;
    Beta  = 0.0;
  }
  under Wall {
    Type = Adiabatic;
    Integration = Full;
  }
}

under Space {
  under NavierStokes {
    Flux = Roe;
    Reconstruction = Linear;
    AdvectiveOperator = FiniteVolume;
    Limiter = VanAlbada;
    Gradient = LeastSquares;
    Dissipation = SecondOrder;
    Beta = 0.33333333333;
    Gamma = 1.0;
  }
  under Boundaries {
    Type = StegerWarming;
  }
}

under Forced {
  Type = Deforming;
  Frequency = 0.05;
  TimeStep = 0.05;
  under Deforming {
    Position = "../sources/Surf_def1.top";
    Amplification = 1.0;
  }
}

//under Surfaces {
//  under SurfaceData[3] {
//    Nx = 1.0;
//    Ny = 0.0;
//    Nz = 0.0;
//  }
//}

under MeshMotion {
  Type = Corotational;
  Element = TorsionalSprings;
//  Element = NonLinearBallVertex;
  NumIncrements = 1;
  under Newton {
    MaxIts = 1;
    under LinearSolver {
      Type = Gmres;
      MaxIts = 200;
      KrylovVectors = 200;
      Eps = 1e-6;
      Output = "log/MMconv.out";
      under Preconditioner { Type = Jacobi; Fill = 0; }
    }
  }
//  under Symmetry {
//    Nx = 0.;
//    Ny = 0.;
//    Nz = 1.;
//  }
}

under Time {
  TimeStep = 0.05;
  MaxIts = 200;
  MaxTime = 100;
  Prec = NonPreconditioned;
  Type = Implicit;
  under Implicit {
    Type = ThreePointBackwardDifference;
    MatrixVectorProduct = Approximate;
    under Newton {
      MaxIts = 20;
      Eps = 0.001;
      FailSafe = AlwaysOn;
      under LinearSolver {
        under NavierStokes {
          Type = Gmres;
          MaxIts = 250;
          KrylovVectors = 250;
          Eps = 0.0001;
          Output = "log/NSconv.out";
          under Preconditioner {
            Type = Ras;
            Fill = 0;
          }
        }
      }
    }
  }
}

under EmbeddedFramework {
  Intersector = PhysBAM;
  SurrogateSurface = ReconstructedSurface;//ControlVolumeFace;
  RiemannNormal = Structure;
  ViscousInterfaceOrder = SecondOrder;
}
