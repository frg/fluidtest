#!/bin/bash

# This script will generate the baseline for unit test: EMBALE_Mesh

./run.EMBALE_Mesh
mv -f *.dat ../baseline/EMBALE_Mesh
mv -f simulations/log/aerof.out ../baseline/EMBALE_Mesh/aerof.out
mv -f simulations/log/NSconv.out ../baseline/EMBALE_Mesh/NSconv.out
mv -f simulations/log/MMconv.out ../baseline/EMBALE_Mesh/MMconv.out
