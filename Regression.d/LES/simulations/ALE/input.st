under Problem {
  Type   = Unsteady;
  Mode   = NonDimensional;
  Prec   = LowMach;
}

under Input {
 GeometryPrefix        = "../../data/ALE/prolate";
// Solution      = "references/Solution.bin";
// RestartData   = "references/Restart.data";
}

under Output {
  under Postpro {
    Prefix    = "postpro/";
//    Density   = "Density.bin";
//    Velocity  = "Velocity.bin";
    Pressure  = "Pressure.bin";
//    Force     = "../results/Force.out";
    LiftandDrag  = "../results/LiftandDrag.out";
    Frequency = 0;
  }
  under Restart {
    Frequency = 0;
    Prefix    = "references/";
    Solution  = "Solution.bin";
    RestartData = "Restart.data";
  }
}

under Equations {
  Type = NavierStokes;
  under FluidModel[0] {
    Fluid = PerfectGas;
    under GasModel{
      SpecificHeatRatio = 1.4;
      PressureConstant = 0.0;
    }
  }
  under ViscosityModel{
    Type = Constant;
  }
  under TurbulenceClosure {
    Type = LESModel;
    under LESModel{
      Type = Smagorinsky;
      Delta = Volume;
    }
    under Tripping{
     under Box1{
         X0 = 0.274;
         Y0 = -100;
         Z0 = -100;
         X1 = 100;
         Y1 = 100;
         Z1 = 100;
     }
    }
  }  
}

under ReferenceState {
  Reynolds = 40000;
  Length = 1.0;     
}

under BoundaryConditions {
  under Inlet {
    Mach     = 0.15;
    Alpha    = 0.0;
    Beta     = 20.0;
  }
  under Wall {
    Type = Adiabatic;
    Integration = Full;
//    Integration = WallFunction;
//    Delta = 0.0075;
  }
}

under Space  {
  under NavierStokes { 
    Flux           = Roe;
    Reconstruction = Linear;
    Gradient       = LeastSquares;
    Dissipation    = SecondOrder;
    Beta           = 0.33333;
    Gamma          = 1.0;
  }
}

under Preconditioner {
  Mach = 0.15;
  k = 1.0;
}

under Time  {
  Type   = Implicit;
  MaxIts = 500;
  Cfl0   = 1.0;
  Cfl1   = 5.0; 
  Cfl2   = 5.0;
  CflMax = 50.0;
  Ser    = 1.0;
//   TimeStep = 0.0005;
  under Implicit {
    Type = ThreePointBackwardDifference;
    MatrixVectorProduct = Approximate;
    under Newton {
      MaxIts = 10;
      Eps    = 0.01;
      under LinearSolver { 
        under NavierStokes { 
          Type          = Gmres;
          EpsFormula    = Eisenstadt;
          MaxIts        = 100;
          KrylovVectors = 100;
          Eps           = 0.0001;
          CheckFinalRes = Yes;
	  under Preconditioner { Type = Ras; Fill = 0; }
        }
      }
    }
  }
}
