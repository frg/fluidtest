#!/bin/bash

rm -f *.dat
rm -f simulations/ALE/postpro/Pressure.* simulations/ALE/results/* simulations/ALE/log/* simulations/ALE/references/*
rm -f simulations/Embedded/postpro/Pressure.* simulations/Embedded/results/* simulations/Embedded/log/* simulations/Embedded/references/*
rm -f reg.out
