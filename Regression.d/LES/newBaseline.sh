#!/bin/bash

# This script will generate the baseline for unit test: LES

./run.LES
mv -f *.dat ../baseline/LES
mv -f simulations/ALE/log/aerof.out ../baseline/LES/ALE_aerof.out
mv -f simulations/Embedded/log/aerof.out ../baseline/LES/Embedded_aerof.out
