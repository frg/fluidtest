#!/bin/bash

rm -f aerof aeros matcher partnmesh sower 
rm -Rf CMakeFiles cmake_install.cmake CTestTestfile.cmake Makefile
rm -f Discrepancies.pdf Discrepancies.ps gnuplot_create reg_test_summary
cd test1
./clean.sh
cd ../test2
./clean.sh
cd ../ALEViscous
./clean.sh
cd ../EMBViscous
./clean.sh
cd ../EMBALE_Mesh
./clean.sh
cd ../LES
./clean.sh
cd ../AgardLinearized
./clean.sh
cd ../F16_airfoil
./clean.sh
cd ../Sensitivities
./clean.sh
cd ..
